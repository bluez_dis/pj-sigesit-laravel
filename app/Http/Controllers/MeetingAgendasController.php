<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MeetingAgenda;

class MeetingAgendasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("meeting_agendas.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $meeting_agenda = new MeetingAgenda;
        $meeting_agenda->meeting_id = $input['meeting_id'];
        $meeting_agenda->name = $input['name'];
        $meeting_agenda->description = $input['description'];
        // $meeting_agenda->ordering = $input['ordering'];
        $meeting_agenda->konklusi = $input['konklusi'];
        $meeting_agenda->intervensi = $input['intervensi'];
        $meeting_agenda->created_by = $input['created_by'];
        $meeting_agenda->save();
        \Session::flash('success','Agenda berhasil dibuat');
        return redirect("meetings/".$meeting_agenda->meeting_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $meeting_agenda = MeetingAgenda::find($id);
        return view("meeting_agendas.show",compact(["meeting_agenda"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meeting_agenda = MeetingAgenda::find($id);
        return view("meeting_agendas.edit",compact(["meeting_agenda"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $meeting_agenda = MeetingAgenda::find($id);
        $meeting_agenda->meeting_id = $input['meeting_id'];
        $meeting_agenda->name = $input['name'];
        $meeting_agenda->description = $input['description'];
        // $meeting_agenda->ordering = $input['ordering'];
        $meeting_agenda->konklusi = $input['konklusi'];
        $meeting_agenda->intervensi = $input['intervensi'];
        $meeting_agenda->updated_by = $input['updated_by'];
        $meeting_agenda->save();
        \Session::flash('success','Agenda berhasil diupdate');
        return redirect("meetings/".$meeting_agenda->meeting_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meeting_agenda = MeetingAgenda::find($id);
        $meeting_agenda->delete();
        \Session::flash('success','Agenda berhasil di Hapus');

        return redirect("meetings/".$meeting_agenda->meeting_id);
    }
}
