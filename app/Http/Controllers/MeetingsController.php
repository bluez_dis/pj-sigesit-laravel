<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Meeting;
use App\Models\MeetingDocument;
use File;
use View;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MeetingExport;
use Carbon\Carbon;

class MeetingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meetings = Meeting::orderBy('date', 'desc')->get();
        return view("meetings.index",compact(["meetings"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $meetings = Meeting::all();
        return view("meetings.create",compact(["meetings"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $count_meeting = Meeting::count();
        $date_now = Carbon::now();

        $input = $request->all();
        $meeting = new Meeting;
        $meeting->name = $input['name'];
        $meeting->description = $input['description'];
        $meeting->ordering = $count_meeting + 1;
        if(isset($input['date'])){
            $meeting->date = date("Y-m-d", strtotime($input['date']));
        }else{
            $meeting->date = date('Y-m-d', strtotime($date_now));
        }
        if(isset($input['image'])){
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/meetings/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/meetings/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $meeting->image = $destinationPath.'/'.$filename;
            }
        }        
        $meeting->created_by = $input['created_by'];
        $meeting->save();

            $i = 0;
            $count_looping = $input['count_looing_product_image'];
            for ($x = 0; $x < $count_looping; $x++) {                
                $meeting_document = new MeetingDocument;
                $meeting_document->meeting_id = $meeting->id;
                $meeting_document->name = $input['name_document'][$i];
                // upload image
                if(isset($input['document'][$i])){
                    $files = $input['document'][$i];
                    if ($files) {
                        $destinationPath    = 'uploads/attachment/meeting_documents/'; // The destination were you store the document.
                        if(!(file_exists(public_path('/uploads/attachment/meeting_documents/'))))
                        {
                            File::makeDirectory($destinationPath, $mode = 0777, true, true);
                        }
                        $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                        $mime_type          = $files->getMimeType(); // Gets this example image/png
                        $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                        $filename           = time().'-'.$filename; // random file name to replace original
                        $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                        $meeting_document->document = $destinationPath.'/'.$filename;
                    }
                }
                $meeting_document->save();
                $i++;
            }
        
        \Session::flash('success','Sidang berhasil dibuat');
        return redirect("meetings/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $meeting = Meeting::find($id);
        return view("meetings.show",compact(["meeting"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $meeting = Meeting::find($id);
        return view("meetings.edit",compact(["meeting"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $date_now = Carbon::now();
        
        $meeting = Meeting::find($id);
        $meeting->name = $input['name'];
        $meeting->description = $input['description'];
        if(isset($input['date'])){
            $meeting->date = date("Y-m-d", strtotime($input['date']));
        }else{
            $meeting->date = date('Y-m-d', strtotime($date_now));
        }
        $meeting->updated_by = $input['updated_by'];

        if ($request->file('image')) {
            if(!is_null($meeting->image)){
                unlink($meeting->image);                
            }
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/meetings/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/meetings/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $meeting->image = $destinationPath.'/'.$filename;
            }            
        }
        
        if ($request->file('document_report')) {
            if(!is_null($meeting->document_report)){
                unlink($meeting->document_report);                
            }
            $files = $input['document_report'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/meetings/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/meetings/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $meeting->document_report = $destinationPath.'/'.$filename;
            }            
        }

        $meeting->save();
        \Session::flash('success','Sidang berhasil di update');
        return redirect("meetings/");
    }

    public function export_excel($id)
	{
        $meeting = Meeting::find($id);
        $dg = array();
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(public_path('tamplate/TABEL_POSISI_SIDANG.docx'));
        $templateProcessor->setValue("meeting_name",htmlspecialchars($meeting->name));

        $i = 0;
        foreach ($meeting->agendas as $agenda) {
            $comments = '';
            foreach ($agenda->comments as $comment) {
                $comments .= $comment->comment. ', ';   
            }
            $dg[] = ["name" => $agenda->name, "description" => $agenda->description, "konklusi" => $agenda->konklusi, "intervensi" => $agenda->intervensi, "comments" => $comments];
            $i = $i + 1;            
        }        

        $templateProcessor->cloneRow('name', count($dg));  

        $parser = new \HTMLtoOpenXML\Parser();
        for($i = 1; $i <= count($dg); $i++){           
            $templateProcessor->setValue("no#".$i, htmlspecialchars($i));
            $templateProcessor->setValue("name#".$i, htmlspecialchars($dg[$i-1]["name"]));
            $description = $parser->fromHTML($dg[$i-1]["description"]);
            $templateProcessor->setValue("description#".$i, strip_tags(htmlspecialchars_decode($description)));

            $comments = $parser->fromHTML($dg[$i-1]["comments"]);
            $templateProcessor->setValue("comments#".$i, strip_tags(htmlspecialchars_decode($comments)));
            
            $konklusi = $parser->fromHTML($dg[$i-1]["konklusi"]);
            $templateProcessor->setValue("konklusi#".$i, strip_tags(htmlspecialchars_decode($konklusi)));            
            $intervensi = $parser->fromHTML($dg[$i-1]["intervensi"]);
            $templateProcessor->setValue("intervensi#".$i, strip_tags(htmlspecialchars_decode($intervensi)));            
        }

        $templateProcessor->saveAs(public_path('download/TABEL_POSISI_SIDANG.docx'));
        return response()->download(public_path('download/TABEL_POSISI_SIDANG.docx'));
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meeting = Meeting::find($id);
        if(!is_null($meeting->image)){
            unlink($meeting->image);
        }
        if(!is_null($meeting->document_report)){
            unlink($meeting->document_report);
        }
        $meeting->delete();
        \Session::flash('success','Sidang berhasil di Hapus');

        return redirect("meetings/");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create_document_meetings($id)
    {
        $meeting = Meeting::find($id);
        return view("meetings.create_document",compact(["meeting"]));
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_document_meetings(Request $request)
    {
        $input = $request->all();
        $meeting_document = new MeetingDocument;
        $meeting_document->name = $input['name'];
        $meeting_document->meeting_id = $input['meeting_id'];
        // upload image
        $files = $input['document'];
        if ($files) {
            $destinationPath    = 'uploads/attachment/meeting_documents/'; // The destination were you store the document.
            if(!(file_exists(public_path('/uploads/attachment/meeting_documents/'))))
            {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
            $mime_type          = $files->getMimeType(); // Gets this example document/png
            $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
            $filename           = time().'-'.$filename; // random file name to replace original
            $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
            $meeting_document->document = $destinationPath.'/'.$filename;
        }
        $meeting_document->save();
        \Session::flash('success','Dokumen berhasil dibuat');
        return redirect("meetings/".$meeting_document->meeting_id);
    }

    public function download_document_meetings(Request $request, $id){                       
        $input = $request->all();
        $meeting_document = MeetingDocument::find($id);
        return response()->download($meeting_document->document);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_document_report(Request $request)
    {
        $input = $request->all();        
        $meeting = Meeting::find($input['meeting_id']);
        
        if ($request->file('document_report')) {
            if(!is_null($meeting->document_report)){
                unlink($meeting->document_report);                
            }
            $files = $input['document_report'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/meetings/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/meetings/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $meeting->document_report = $destinationPath.'/'.$filename;
            }            
        }

        $meeting->save();
        \Session::flash('success','Laporan sidang berhasil di upload');
        return redirect("meetings/".$meeting->id);
    }

    public function download_document_report(Request $request, $id){                       
        $input = $request->all();
        $meeting = Meeting::find($id);
        return response()->download($meeting->document_report);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete_meeting_document($id)
    {
        $meeting_document = MeetingDocument::find($id);
        if(!is_null($meeting_document->document)){
            // remove document
            unlink($meeting_document->document);
        }
        $meeting_document->delete();
        \Session::flash('success','Dokumen berhasil dihapus');
        return redirect("meetings/".$meeting_document->meeting_id);
    }
}
