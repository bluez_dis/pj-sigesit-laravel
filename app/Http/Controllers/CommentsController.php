<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\MeetingAgenda;

class CommentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $meeting_agenda = MeetingAgenda::find(request()->get('meeting_agenda_id'));
        $comments = $meeting_agenda->comments;
        return view("comments.create", compact(["comments", "meeting_agenda"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $comment = new Comment;
        $comment->meeting_agenda_id = $input['meeting_agenda_id'];
        $comment->comment = $input['comment'];  
        $comment->created_by = $input['created_by'];
        $comment->save();
        \Session::flash('success','Komentar berhasil dibuat');
        return redirect("comments/create?meeting_agenda_id=".$comment->meeting_agenda_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update_meeting_agenda(Request $request)
    {
        $input = $request->all();
        $meeting_agenda = MeetingAgenda::find($input['meeting_agenda_id']);
        $meeting_agenda->konklusi = $input['konklusi'];
        $meeting_agenda->intervensi = $input['intervensi'];
        $meeting_agenda->updated_by = $input['updated_by'];
        $meeting_agenda->save();
        \Session::flash('success','Agenda berhasil diupdate');
        return redirect("comments/create?meeting_agenda_id=".$meeting_agenda->id);
    }    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::find($id);
        return view("comments.show",compact(["comment"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::find($id);
        return view("comments.edit",compact(["comment"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $comment = Comment::find($id);
        $comment->meeting_agenda_id = $input['meeting_agenda_id'];
        $comment->comment = $input['comment'];
        $comment->updated_by = $input['updated_by'];
        $comment->save();
        \Session::flash('success','Komentar berhasil diupdate');
        return redirect("meetings/".$comment->meeting_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        $comment->delete();
        \Session::flash('success','Komentar berhasil di Hapus');

        return redirect("meetings/".$comment->meeting_id);
    }
}
