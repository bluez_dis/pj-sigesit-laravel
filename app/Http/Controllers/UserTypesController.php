<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserType;

class UserTypesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_types = UserType::all();
        return view("user_types.index",compact(["user_types"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("user_types.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $user_type = new UserType;
        $user_type->name = $input['name'];        
        $user_type->created_by = $input['created_by'];        
        $user_type->save();
        \Session::flash('success','Tipe User berhasil dibuat');
        return redirect("user_types/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_type = UserType::find($id);
        return view("user_types.edit",compact(["user_type"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user_type = UserType::find($input["id"]);
        $user_type->name = $input["name"];
        $user_type->updated_by = $input["updated_by"];        
        $user_type->save();
        \Session::flash('success','Tipe User berhasil di update');
        return redirect("user_types/");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_type = UserType::find($id);
        $user_type->delete();
        \Session::flash('success','Tipe User berhasil di dalete');

        return redirect("user_types/");
    }
}
