<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserType;
use File;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view("users.index",compact(["users"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_types = UserType::all();
        return view("users.create",compact(["user_types"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $user = new User;
        $user->name = $input['name'];
        $user->user_type_id = $input['user_type_id'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        if ($request->file('image')) {
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/users/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/users/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $user->image = $destinationPath.'/'.$filename;
            }
        }
        $user->save();
        \Session::flash('success','User berhasil dibuat');
        return redirect("users/");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_types = UserType::all();
        $user = User::find($id);
        return view("users.edit",compact(["user", "user_types"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = User::find($input["id"]);
        $user->name = $input["name"];
        $user->user_type_id = $input['user_type_id'];
        $user->email = $input["email"];
        if (!empty($input["password"])) {
            $user->password = bcrypt($input["password"]);
        }
        if ($request->file('image')) {
            if(!is_null($user->image)){
                unlink($user->image);                
            }
            $files = $input['image'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/users/'; // The destination were you store the document.
                if(!(file_exists(public_path('/uploads/attachment/users/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $user->image = $destinationPath.'/'.$filename;
            }            
        }
        $user->save();
        \Session::flash('success','User berhasil di update');
        return redirect("users/");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if(!is_null($user->image)){
            unlink($user->image);                
        }
        $user->delete();
        \Session::flash('success','User berhasil di dalete');

        return redirect("users/");
    }
}
