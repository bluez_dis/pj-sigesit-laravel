<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Meeting;
use App\Models\MeetingAgenda;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {        
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count_meeting = Meeting::count();
        $count_meeting_not_conclusion =  0;
        $meeting_agendas = MeetingAgenda::all();
        $agenda_count = MeetingAgenda::count();

        foreach ($meeting_agendas as $meeting_agenda){
            if ($meeting_agenda->konklusi == "" && $meeting_agenda->intervensi == ""){
                $count_meeting_not_conclusion = $count_meeting_not_conclusion + 1;
            }            
        }
        $count_meeting_conclusion =  $meeting_agendas->count() - $count_meeting_not_conclusion;
    
        return view('home',compact(["count_meeting", "agenda_count", "count_meeting_conclusion", "count_meeting_not_conclusion"]));
    }
}
