<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    use HasFactory;

    public function agendas()
    {
        return $this->hasMany('App\Models\MeetingAgenda')->orderBy('created_at');
    }

    public function document_meetings()
    {
        return $this->hasMany('App\Models\MeetingDocument');
    }
}
