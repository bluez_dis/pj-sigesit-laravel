<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }

    public function meeting_agenda()
    {
        return $this->belongsTo('App\Models\MeetingAgenda', 'meeting_agenda_id');
    }
}
