<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Meeting;
use Illuminate\Contracts\View\View;

class MeetingExport implements FromView
{
    protected $id;

    function __construct($id) {
           $this->id = $id;
    }

    public function view(): View
    {
        return view('meetings.excel', [
            'meeting' => Meeting::find($this->id)
        ]);
    }
}
