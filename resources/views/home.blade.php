@extends('layouts.app')

@section('content')
<!-- Dashboard -->
<div class="dashboard-container">
    <div class="row">
        <div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12">
            <div class="widget widget-seven background-success">
                <div class="widget-body">
                    <div href="javascript: void(0);" class="widget-body-inner">
                        <h7 class="text-uppercase">Total Seluruh Sidang</h5>
                        <i class="counter-icon fa fa-institution"></i>
                        <span class="counter-count">
                            <!-- <i class="icmn-arrow-up5"></i> -->
                            <span class="counter-init" data-from="0" data-to="{{$count_meeting}}"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12">
            <div class="widget widget-seven background-success">
                <div class="widget-body">
                    <div href="javascript: void(0);" class="widget-body-inner">
                        <h7 class="text-uppercase">Total Seluruh Agenda</h5>
                        <i class="counter-icon fa fa-institution"></i>
                        <span class="counter-count">
                            <!-- <i class="icmn-arrow-up5"></i> -->
                            <span class="counter-init" data-from="0" data-to="{{$agenda_count}}"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12">
            <div class="widget widget-seven background-default">
                <div class="widget-body">
                    <div href="javascript: void(0);" class="widget-body-inner">
                        <h7 class="text-uppercase">Total Agenda ada Kertas Posisi</h5>
                        <i class="counter-icon fa fa-sticky-note"></i>
                        <span class="counter-count">
                            <!-- <i class="icmn-arrow-down5"></i> -->
                            <span class="counter-init" data-from="0" data-to="{{$count_meeting_conclusion}}"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-sm-6 col-xs-12">
            <div class="widget widget-seven">
                <div class="widget-body">
                    <div href="javascript: void(0);" class="widget-body-inner">
                        <h7 class="text-uppercase">Total Agenda Belum ada Kertas Posisi</h5>
                        <i class="counter-icon fa fa-sticky-note-o"></i>
                        <span class="counter-count">
                            <!-- <i class="icmn-arrow-up5"></i> -->
                            <span class="counter-init" data-from="0" data-to="{{$count_meeting_not_conclusion}}"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <br>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>    
</div>

<!-- End Dashboard -->
@endsection