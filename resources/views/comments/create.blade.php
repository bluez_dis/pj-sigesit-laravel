@extends('layouts.app')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Agenda : {{ $meeting_agenda->name }}</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="margin-bottom-50">                    
                    <!-- Horizontal Form -->                    
                    <div class="panel-heading">
                    <h4>List Dokument</h4>
                    </div>
                    <div class="panel-body">
                        <div class="conversation-block height-600 custom-scroll">
                        @foreach($meeting_agenda->meeting->document_meetings as $document_meeting)                                                    
                        <div class="panel-heading collapsed" role="tab" id="{{ $document_meeting->id }}" data-toggle="collapse" data-parent="#accordion" data-target="#{{ $document_meeting->id+1 }}" aria-expanded="true" aria-controls="collapseOne">
                            <span>{{$document_meeting->name}} </span>
                        </div>
                        <div id="{{ $document_meeting->id+1 }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{ $document_meeting->id }}">
                            @if(substr($document_meeting->document, strpos($document_meeting->document, ".") + 1) == 'jpg' || substr($document_meeting->document, strpos($document_meeting->document, ".") + 1) == 'png')
                                <img src="/{{$document_meeting->document}}"/>
                            @elseif(substr($document_meeting->document, strpos($document_meeting->document, ".") + 1) == 'pdf')
                                <iframe src="/{{$document_meeting->document}}" frameborder="0" style="width:100%;min-height:640px;"></iframe>
                            @elseif(substr($document_meeting->document, strpos($document_meeting->document, ".") + 1) == 'docx' || substr($document_meeting->document, strpos($document_meeting->document, ".") + 1) == 'doc')
                                <iframe class="doc" src="https://docs.google.com/gview?url=http://padi-sigesit.kamimengundang.id//{{$document_meeting->document}}&embedded=true" style="width:100%;min-height:640px;"></iframe>
                            @else
                            format file tidak di kenal 
                            @endif                            
                        </div>
                        @endforeach
                        </div>
                    </div>
                    <!-- End Horizontal Form -->
                </div>
                
            </div>
            <div class="col-lg-6">
                <div class="margin-bottom-50">
                    <h4>Form Komentar</h4>
                    <br />
                    <!-- Horizontal Form -->                    
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="conversation-block height-400 custom-scroll">
                                    @foreach($comments as $comment)                                    
                                    @if(Auth::user()->id == $comment->created_by)
                                    <div class="conversation-item you">
                                        <div class="s1">
                                            <a class="avatar" href="javascript:void(0);">
                                                @if($comment->user->image)
                                                <img src="/{{$comment->user->image}}" alt="Alternative text to the image">
                                                @esle
                                                <img src="/cleanui/assets/common/img/temp/avatars/4.jpg" alt="Alternative text to the image">                                                
                                                @endif
                                            </a>
                                        </div>
                                        <div class="s2">
                                            <strong>{{ $comment->user ? $comment->user->name : "" }}</strong>
                                            <p>{!! $comment->comment !!}</p>
                                        </div>
                                    </div>
                                    @else
                                    <div class="conversation-item">
                                        <div class="s1">
                                            <a class="avatar" href="javascript:void(0);">
                                                @if($comment->user->image)
                                                <img src="/{{$comment->user->image}}" alt="Alternative text to the image">
                                                @esle
                                                <img src="/cleanui/assets/common/img/temp/avatars/4.jpg" alt="Alternative text to the image">                                                
                                                @endif
                                            </a>
                                        </div>
                                        <div class="s2">
                                            <strong>{{ $comment->user ? $comment->user->name : "" }}</strong>
                                            <p>{!! $comment->comment !!}</p>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                                <div class="form-group padding-top-20 margin-bottom-0">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('comments') }}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" class="form-control" name="created_by" id="created_by" value="{{ Auth::user()->id }}">
                                        <input type="hidden" class="form-control" name="meeting_agenda_id" id="meeting_agenda_id" value="{{ request()->get('meeting_agenda_id') }}">
                                        <textarea class="form-control adjustable-textarea summernote" name="comment" id="comment" placeholder="Type and press enter" style="overflow-x: hidden; word-wrap: break-word; resize: none; overflow-y: visible;"></textarea>
                                        <button class="btn btn-primary width-200 margin-top-10">
                                            <i class="fa fa-send margin-right-5"></i>
                                            Send
                                        </button>
                                    </form>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Horizontal Form -->
                </div>
                
            </div>
        </div>        
</section>
<!-- End -->


<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Agenda : {{ $meeting_agenda->name }}</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <h4>Form Agenda (konklusi / Intervensi)</h4>
                    <br />
                    <!-- Horizontal Form -->                    
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/comments/update_meeting_agenda') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" class="form-control" name="updated_by" id="updated_by" value="{{ Auth::user()->id }}">
                                    <input type="hidden" class="form-control" name="meeting_agenda_id" id="meeting_agenda_id" value="{{ request()->get('meeting_agenda_id') }}">
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label class="form-control-label" for="l0">Konklusi</label>
                                        </div>
                                        <div class="col-md-9">
                                            <textarea class="form-control summernote" id="konklusi" name="konklusi" placeholder="Input Konklusi" rows="3" id="l15">{{$meeting_agenda->konklusi}}</textarea>
                                            @if ($errors->has('konklusi'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('konklusi') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label class="form-control-label" for="l0">Intervensi</label>
                                        </div>
                                        <div class="col-md-9">
                                            <textarea class="form-control summernote" id="intervensi" name="intervensi" placeholder="Input Intervensi" rows="3" id="l15">{{$meeting_agenda->intervensi}}</textarea>
                                            @if ($errors->has('intervensi'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('intervensi') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                                        
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Update') }}
                                            </button>
                                            <a class="btn btn-success" href="/meetings/{{ $meeting_agenda->meeting_id }}">Back</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End Horizontal Form -->
                </div>
                
            </div>
        </div>        
</section>
<!-- End -->

<script>
    $(function(){
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'LL'
        });

        $('.summernote').summernote({
            height: 150
        });
    })
</script>
@endsection