@extends('layouts.app')

@section('content')
<section class="page-content">
<div class="page-content-inner">    
    <!--  -->
    <section class="panel">
        <div class="panel-heading">
            <h3>
            Mengelola Sidang
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <div class="col-md-4">
                        <a href="/meetings/create" class="btn btn-primary">
                            Tambah Sidang Baru
                        </a>                        
                    </div>
                    
                    <div class="col-xs-12">&nbsp;</div>                    
                    <div class="margin-bottom-50">
                        <table class="table table-hover nowrap" id="example2" width="100%">
                            <thead>
                              <tr>
                                <th>Nama Sidang</th>
                                <th>Tanggal Sidang</th>
                                <th width="15%">Aksi</th>
                              </tr>
                            <tbody>
                                @foreach($meetings as $meeting)
                                <tr>
                                    <td>{{ substr($meeting->name, 0,  60) }}</td>
                                    <td>{{ date("d-M-Y", strtotime($meeting->date)) }}</td>
                                    <td>
                                        <a href='/meetings/{{$meeting->id}}/export_excel' class="btn btn-info btn-xs" target="_blank">Cetak Kertas Posisi</a>
                                        @if(!is_null($meeting->document_report))
                                        <a href="{{url('/meetings/download_document_report/'.$meeting->id)}}" class="btn btn-info btn-xs">
                                            Unduh Laporan Sidang
                                        </a>
                                        @endif
                                        <a class='btn btn-success btn-xs' href='{{url('meetings/'.$meeting->id)}}'>Detail</a> 
                                        <a class='btn btn-primary btn-xs' href='{{url('meetings/'.$meeting->id.'/edit')}}'>Edit</a>
                                        <a class='btn btn-danger btn-xs open-confirm-hapus' onclick="return confirm('Anda Yakin Hapus Data Ini?')" href='{{url('meetings/delete')}}/{{$meeting->id}}'>Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End  -->

</div>

<!-- Page Scripts -->
<script>
    $(function(){
        $('.datepicker').datetimepicker({
            format:  "YYYY-MM-DD"
        });

        ///////////////////////////////////////////////////////////
        // DATATABLES
        $('#example2').DataTable({
            responsive: true,
            "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
            "ordering": false
        });
    });
</script>
<!-- End Page Scripts -->
</section>
@endsection