@extends('layouts.app')


@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Halaman Detail Sidang</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <br />
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <!-- Horizontal Form -->                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('meetings.update', $meeting->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="hidden" class="form-control" name="updated_by" id="updated_by" value="{{ Auth::user()->id }}">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  placeholder="Input Nama Sidang" value="{{$meeting->name}}" required autofocus disabled>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Keterangan Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <p>
                                {!! $meeting->description !!}
                                </p>                                
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Gambar Sidang</label>
                            </div>
                            <div class="col-md-9">
                                @if($meeting->image)
                                <img id="image" src="/{{$meeting->image}}" alt="{{$meeting->name}}" style="width:100%;max-width:300px">
                                @else
                                <p class="text-red">* gambar belum tersedia</p>
                                @endif
                            </div>
                        </div>

                        @if($meeting->document_report)
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Laporan Sidang</label>
                            </div>
                            <div class="col-md-2">                                
                                <a href="{{url('/meetings/download_document_report/'.$meeting->id)}}" class="btn btn-info">
                                    Download
                                </a>
                            </div>                            
                        </div>
                        @else
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Laporan Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <p class="text-red">* laporan sidang tersedia</p>
                            </div>                            
                        </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tanggal Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="input-group">
                                        <input type="text" id="date"  class="form-control" name="date" placeholder="Input Tanggal Lahir" value="{{date('d-M-Y', strtotime($meeting->date))}}" autofocus disabled>
                                        <span class="input-group-addon">
                                            <i class="icmn-calendar"></i>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>                                                                      
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<!-- End -->

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>List Agenda</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">                
                <div class="col-md-4">
                    <a href="/meeting_agendas/create?meeting_id={{$meeting->id}}" class="btn btn-primary">
                        Tambah Agenda Sidang
                    </a>
                </div>
                <div class="col-xs-12">&nbsp;</div>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <table class="table table-hover nowrap" id="example3" width="100%">
                            <thead>
                              <tr>
                                <th width="35">Nama Agenda</th>
                                <th>Tanggal Agenda</th>
                                <th width="5">Aksi</th>
                              </tr>
                            <tbody>
                                @foreach($meeting->agendas as $agenda)
                                <tr>
                                    <td><a href='{{url('meeting_agendas/'.$agenda->id)}}'>{{ substr($agenda->name, 0,  100) }}</a> </td>
                                    <td>{{ date("d-M-Y", strtotime($agenda->created_at)) }}</td>
                                    <td>
                                        <a class='btn btn-success btn-xs' href='{{url('comments/create?meeting_agenda_id='.$agenda->id)}}'>Komentar</a> 
                                        <a class='btn btn-success btn-xs' href='{{url('meeting_agendas/'.$agenda->id)}}'>Detail</a> 
                                        <a class='btn btn-primary btn-xs' href='{{url('meeting_agendas/'.$agenda->id.'/edit')}}'>Edit</a>
                                        <a class='btn btn-danger btn-xs open-confirm-hapus' onclick="confirm('Are you sure?')" href='{{url('meeting_agendas/delete')}}/{{$agenda->id}}'>Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<!-- End -->

<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>List Dokumen</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                @if ($message = Session::get('success_document'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                <div class="col-md-4">
                    <a href="/meetings/create_document_meetings/{{$meeting->id}}" class="btn btn-primary">
                        Tambah Dokumen
                    </a>
                </div>
                <div class="col-xs-12">&nbsp;</div>
                <div class="margin-bottom-50">
                    <br />
                    <!-- Horizontal Form -->
                    <table class="table table-hover nowrap" id="example2" width="100%">
                            <thead>
                              <tr>
                                <th>Nama Dokumen</th>
                                <th>Action</th>
                              </tr>
                            <tbody>
                                @foreach($meeting->document_meetings as $document_meeting)
                                <tr>
                                    <td>{{$document_meeting->name}}</td>
                                    <td>
                                        @if($document_meeting->document != null)
                                        <a href="{{url('/meetings/download_document_meetings/'.$document_meeting->id)}}" class="btn btn-info btn-xs">
                                            Download
                                        </a>
                                        <a href="{{url('/meetings/delete_meeting_document/'.$document_meeting->id)}}" class="btn btn-danger btn-xs">
                                            Delete
                                        </a>
                                        @else
                                            File Tidak tersedia
                                        @endif
                                    </td>                                 
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <a href='/meetings/{{$meeting->id}}/export_excel' class="btn btn-info" target="_blank">Cetak Kertas Posisi</a>
        <a class='btn btn-primary' href='{{url('meetings/'.$meeting->id.'/edit')}}'>Edit</a>
        <a class="btn btn-success" href="/meetings">Back</a>
        @if(is_null($meeting->document_report))
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_document_report">
            Upload Laporan Sidang
        </button>
        <div class="modal fade" id="modal_document_report" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Upload Laporan Sidang </h4>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" id="form" action="{{ url('/meetings/update_document_report') }}" enctype="multipart/form-data">
                    @csrf                    
                    <input type="hidden" class="form-control" name="updated_by" id="updated_by" value="{{ Auth::user()->id }}">
                    <input type="hidden" class="form-control" name="meeting_id" id="updated_by" value="{{ $meeting->id }}">
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama Sidang</label>
                            </div>
                            <div class="col-md-9">
                                {{$meeting->name}}
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Laporan Sidang</label>
                            </div>
                            <div class="col-md-7">
                                <input id="document_report" type="file" class="form-control{{ $errors->has('document_report') ? ' is-invalid' : '' }}" name="document_report" data-max-size="2097152"  placeholder="Input Gambar Sidang" autofocus>

                                @if ($errors->has('document_report'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('document_report') }}</strong>
                                    </span>
                                @endif
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-10">
                                <p class="text-red">* Ukuran Maksimal File 2MB</p>
                                <p class="text-red">* Format File : pdf, docx</p>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
<!-- End -->

<script>
    $(function(){
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'LL'
        });

        ///////////////////////////////////////////////////////////
        // DATATABLES
        $('#example2').DataTable({
            responsive: true,
            "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
            "ordering": false
        });

        ///////////////////////////////////////////////////////////
        // DATATABLES
        $('#example3').DataTable({
            responsive: true,
            "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
            "ordering": false
        });
             
        var document = $('#document_report');
        var maxSize = document.data('max-size');
        $("form").submit(function(){
            if(document.val() != '' && document.get(0).files[0].size>maxSize){
                alert('file dokumen repot sidang melebihi batas ' +maxSize+ ' bytes');
                return false;
            }      
        });
    })
</script>
@endsection