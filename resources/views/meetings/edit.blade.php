@extends('layouts.app')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Edit Sidang</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <h4>Form Sidang</h4>
                    <br />
                    <!-- Horizontal Form -->                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('meetings.update', $meeting->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="hidden" class="form-control" name="updated_by" id="updated_by" value="{{ Auth::user()->id }}">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  placeholder="Input Nama Sidang" value="{{$meeting->name}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if($meeting->image)
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Gambar Sidang</label>
                            </div>
                            <div class="col-md-4">
                                <img id="image" src="/{{$meeting->image}}" alt="{{$meeting->name}}" style="width:100%;max-width:300px">                                
                            </div>
                            <div class="col-md-3">
                                <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" data-max-size="2097152"  placeholder="Input Gambar Sidang" autofocus>

                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-2">
                                <p class="text-red">* Upload jika ingin merubah gambar sidang</p>
                                <p class="text-red">* Ukuran Maksimal File 2MB</p>
                                <p class="text-red">* Format File : jpeg, jpg, png</p>
                            </div>
                        </div>
                        @else
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Gambar Sidang</label>
                            </div>
                            <div class="col-md-7">
                                <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" data-max-size="2097152"  placeholder="Input Gambar Sidang" autofocus>

                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-2">
                                <p class="text-red">* Upload jika ingin merubah gambar sidang</p>
                                <p class="text-red">* Ukuran Maksimal File 2MB</p>
                                <p class="text-red">* Format File : jpeg, jpg, png</p>
                            </div>
                        </div>
                        @endif

                        @if($meeting->document_report)
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Laporan Sidang</label>
                            </div>
                            <div class="col-md-2">                                
                                <a href="{{url('/meetings/download_document_report/'.$meeting->id)}}" class="btn btn-info">
                                    Download
                                </a>
                            </div>
                            <div class="col-md-5">
                                <input id="document_report" type="file" class="form-control{{ $errors->has('document_report') ? ' is-invalid' : '' }}" name="document_report" data-max-size="2097152"  placeholder="Input Gambar Sidang" autofocus>

                                @if ($errors->has('document_report'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('document_report') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-2">
                                <p class="text-red">* Upload jika ingin merubah Laporan sidang</p>
                                <p class="text-red">* Ukuran Maksimal File 2MB</p>
                                <p class="text-red">* Format File : pdf, docx</p>
                            </div>
                        </div>
                        @else
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Laporan Sidang</label>
                            </div>
                            <div class="col-md-7">
                                <input id="document_report" type="file" class="form-control{{ $errors->has('document_report') ? ' is-invalid' : '' }}" name="document_report" data-max-size="2097152"  placeholder="Input Gambar Sidang" autofocus>

                                @if ($errors->has('document_report'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('document_report') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xs-2">
                                <p class="text-red">* Ukuran Maksimal File 2MB</p>
                                <p class="text-red">* Format File : pdf, docx</p>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Keterangan Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <textarea class="form-control summernote" id="description" name="description" placeholder="Input Keterangan Sidang" rows="3" id="l15">{{$meeting->description}}</textarea>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                       

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tanggal Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="input-group datepicker-only-init">
                                        <input type="text" id="date"  class="form-control datepicker-only-init" name="date" value="{{$meeting->date}}" placeholder="Tanggal Sidang"  autofocus/>
                                        <span class="input-group-addon">
                                            <i class="icmn-calendar"></i>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                                              
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                                <a class="btn btn-success" href="/meetings">Back</a>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<!-- End -->

<script>
    $(function(){
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });

        $('.summernote').summernote({
            height: 350
        });

        var image = $('#image');        
        var maxSize = image.data('max-size');
        var document = $('#document_report');
        var maxSizeDocument = document.data('max-size');
        $("form").submit(function(){            
            if(image.val() != '' && image.get(0).files[0].size>maxSize){
                alert('file gambar sidang melebihi batas ' +maxSize+ ' bytes');
                return false;
            }else if(document.val() != '' && document.get(0).files[0].size>maxSizeDocument){
                alert('file dokumen repot melebihi batas ' +maxSize+ ' bytes');
                return false;
            }      
        });
    })
</script>
@endsection