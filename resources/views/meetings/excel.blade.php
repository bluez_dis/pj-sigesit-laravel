<!DOCTYPE html>
<html>
<head>
	<title>Export Laporan Excel Pada Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
 
	<div class="container">
		<center>
			<h4>KERTAS POSISI SIDANG {{$meeting->name}}</h4>
		</center>	
		
		<table class='table table-bordered'>
			<thead>
				<tr>
					<th>No</th>
					<th>Agenda</th>
					<th>Konklusi</th>
					<th>Intervensi</th>
				</tr>
			</thead>
			<tbody>
				@php $i=1 @endphp
				@foreach($meeting->agendas as $s)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{$s->name}}</td>
					<td>{!!$s->konklusi!!}</td>
					<td>{!!$s->intervensi!!}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
 
</body>
</html>