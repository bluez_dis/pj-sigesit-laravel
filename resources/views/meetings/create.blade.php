@extends('layouts.app')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Tambah Sidang</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <h4>Form Sidang</h4>
                    <br />
                    <!-- Horizontal Form -->                    
                    <form class="form-horizontal" role="form" method="POST" id="form" action="{{ url('meetings') }}" enctype="multipart/form-data">
                         @csrf
                        <input type="hidden" class="form-control" name="created_by" id="created_by" value="{{ Auth::user()->id }}">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  placeholder="Input Nama Sidang" autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Gambar Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" data-max-size="2097152"  placeholder="Input Gambar Sidang" autofocus>

                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Keterangan Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <textarea class="form-control summernote" id="description" name="description" placeholder="Input Keterangan Sidang" rows="3" id="l15"></textarea>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                    

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tanggal Sidang</label>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="input-group datepicker-only-init">
                                        <input type="text" id="date"  class="form-control datepicker-only-init" name="date" placeholder="Input Tanggal Sidang" autofocus/>
                                        <span class="input-group-addon">
                                            <i class="icmn-calendar"></i>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <br>
                        <h6>Dokumen Sidang</h6>
                        <div class="form-group row">
                            <div id="dinamic_form">
                                <div id="row_0">              
                                    <div class="box-body">
                                        <div class="row">                
                                        <div class="col-xs-3">
                                            <a id="add" class="btn btn-success btn-sm"> <i class="fa fa-plus"></i></a>
                                            <a id="remove" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i></a>
                                        </div>
                                        <div class="col-xs-3">                  
                                            <input type="text" class="form-control" id="name_document[0]" name="name_document[0]" placeholder="Nama File" required="">
                                        </div>
                                        <div class="col-xs-3">
                                            <input type="file" class="form-control" id="document_0" name="document[0]" data-max-size="2097152" placeholder="Upload File " required="">                                            
                                        </div>                                                                                                
                                        <div class="col-xs-3">
                                            <p class="text-red">* Ukuran Maksimal File 2MB</p>
                                            <p class="text-red">* Format File : pdf, dokumen, rar, zip</p>
                                            <p class="text-red">* Hapus field jika tidak di gunakan</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" name="count_looing_product_image" id="count_looing_product_image" value="1">
                                              
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>
                                <a class="btn btn-success" href="/meetings">Back</a>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<!-- End -->

<script>
    $(function(){
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'YYYY-MM-DD'
        });
    })

    $(document).ready(function() {
      var i = 1;
      $('#add').click(function(){  
          $('#dinamic_form').append('<div id="row_'+i+'"><div class="box-body"><div class="row"><div class="col-xs-3"></div><div class="col-xs-3"><input type="text" class="form-control" id="name_document['+i+']" name="name_document['+i+']" placeholder="Nama File" required=""></div><div class="col-xs-3"><input type="file" class="form-control" id="document['+i+']" name="document['+i+']" placeholder="Upload File " required=""></div><div class="col-xs-2"></div></div></div>');
          ++i;
          $('#count_looing_product_image').val(i);
      });

      $('#remove').click(function(){
        if(i != 0){
            --i;
          }
          $('#count_looing_product_image').val(i);
          $('#row_'+i).remove();               
      });

      $('.summernote').summernote({
            height: 350
        });

        var image = $('#image');        
        var maxSize = image.data('max-size');
        var document = $('#document_0');
        $("form").submit(function(){
            if(image.val() != '' && image.get(0).files[0].size>maxSize){
                alert('file gambar sidang melebihi batas ' +maxSize+ ' bytes');
                return false;
            }else if(document.val() != '' && document.get(0).files[0].size>maxSize){
                alert('file dokumen repot melebihi batas ' +maxSize+ ' bytes');
                return false;
            }      
        });        

    });
</script>
@endsection