@extends('layouts.app')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Edit Agenda</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <h4>Form Agenda</h4>
                    <br />
                    <!-- Horizontal Form -->                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('meeting_agendas.update', $meeting_agenda->id) }}" enctype="multipart/form-data">
                         @csrf
                         @method('PUT')
                        <input type="hidden" class="form-control" name="updated_by" id="updated_by" value="{{ Auth::user()->id }}">
                        <input type="hidden" class="form-control" name="meeting_id" id="meeting_id" value="{{ $meeting_agenda->meeting_id }}">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Agenda</label>
                            </div>
                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  placeholder="Input Agenda" value="{{$meeting_agenda->name}}" autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Keterangan Agenda</label>
                            </div>
                            <div class="col-md-9">
                                <textarea class="form-control summernote" id="description" name="description" placeholder="Input Keterangan" rows="3" id="l15">{{$meeting_agenda->description}}</textarea>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                       

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Konklusi</label>
                            </div>
                            <div class="col-md-9">
                                <textarea class="form-control summernote" id="konklusi" name="konklusi" placeholder="Input Konklusi" rows="3" id="l15">{{$meeting_agenda->konklusi}}</textarea>
                                @if ($errors->has('konklusi'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('konklusi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Intervensi</label>
                            </div>
                            <div class="col-md-9">
                                <textarea class="form-control summernote" id="intervensi" name="intervensi" placeholder="Input Intervensi" rows="3" id="l15">{{$meeting_agenda->intervensi}}</textarea>
                                @if ($errors->has('intervensi'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('intervensi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                              
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                                <a class="btn btn-success" href="/meetings/{{$meeting_agenda->meeting_id}}">Back</a>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<!-- End -->

<script>
    $(function(){
        $('.datepicker-only-init').datetimepicker({
            widgetPositioning: {
                horizontal: 'left'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'LL'
        });

        $('.summernote').summernote({
            height: 350
        });
    })
</script>
@endsection