@extends('layouts.app')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Edit Agenda</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <h4>Form Agenda</h4>
                    <br />
                    <!-- Horizontal Form -->                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('meeting_agendas.update', $meeting_agenda->id) }}" enctype="multipart/form-data">
                         @csrf
                         @method('PUT')
                        <input type="hidden" class="form-control" name="created_by" id="created_by" value="{{ Auth::user()->id }}">
                        <input type="hidden" class="form-control" name="meeting_id" id="meeting_id" value="{{ $meeting_agenda->meeting_id }}">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Agenda</label>
                            </div>
                            <div class="col-md-9">
                                <p>{{$meeting_agenda->name}}</p>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Keterangan Agenda</label>
                            </div>
                            <div class="col-md-9">
                                <p>{!!$meeting_agenda->description!!}</p>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                        

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Konklusi</label>
                            </div>
                            <div class="col-md-9">
                                <p>{!!$meeting_agenda->konklusi!!}</p>
                                @if ($errors->has('konklusi'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('konklusi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Intervensi</label>
                            </div>
                            <div class="col-md-9">
                                <p>{!!$meeting_agenda->intervensi!!}</p>
                                @if ($errors->has('intervensi'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('intervensi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                              
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">                                
                                <a class='btn btn-success' href='{{url('comments/create?meeting_agenda_id='.$meeting_agenda->id)}}'>Komentar</a> 
                                <a class='btn btn-primary' href='{{url('meeting_agendas/'.$meeting_agenda->id.'/edit')}}'>Edit</a>
                                <a class='btn btn-danger open-confirm-hapus' onclick="confirm('Are you sure?')" href='{{url('meeting_agendas/delete')}}/{{$meeting_agenda->id}}'>Delete</a>
                                <a class="btn btn-success" href="/meetings/{{$meeting_agenda->meeting_id}}">Back</a>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<!-- End -->

<script>
    $(function(){
       
    })
</script>
@endsection