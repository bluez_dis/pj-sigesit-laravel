@extends('layouts.app')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Edit Data Tipe User</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <h4>Form Tipe User</h4>
                    <br />
                    <!-- Horizontal Form -->                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('user_types.update', $user_type->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                        <input type="hidden" name="id" value="{{ $user_type->id }}">
                        <input type="hidden" class="form-control" name="updated_by" id="updated_by" value="{{ Auth::user()->id }}">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama User</label>
                            </div>
                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  placeholder="Input Name" value="{{ $user_type->name }}" required autofocus>
                                <input type="hidden" class="form-control" name="created_by" id="updated_by" value="{{ Auth::user()->id }}">

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>                                                             
                         <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                                <a class="btn btn-success" href="/user_types">Back</a>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<!-- End -->
@endsection