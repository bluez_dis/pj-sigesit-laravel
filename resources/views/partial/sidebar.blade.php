<ul class="left-menu-list left-menu-list-root list-unstyled">        
    <li class="left-menu-list-{{ (request()->is('/')) ? 'active' : '' }}">
        <a class="left-menu-link" href="/">
            <i class="left-menu-link-icon icmn-home2"><!-- --></i>
            <span class="menu-top-hidden">Dashboard</span>
        </a>
    </li>
    <li class="left-menu-list-separator "><!-- --></li>    
    <!-- <li class="left-menu-list-submenu">
        <a class="left-menu-link" href="javascript: void(0);">
            <i class="left-menu-link-icon icmn-files-empty2"></i>
            Deafult Pages
        </a>
        <ul class="left-menu-list list-unstyled">
            <li>
                <a class="left-menu-link" href="pages-login-alpha.html">
                    Login Alpha
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="pages-login-beta.html">
                    Login Beta
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="pages-login-omega.html">
                    Login Omega
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="pages-register.html">
                    Register
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="pages-lockscreen.html">
                    Lockscreen
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="pages-pricing-tables.html">
                    Pricing Tables
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="pages-invoice.html">
                    Invoice
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="pages-page-404.html">
                    Page 404
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="pages-page-500.html">
                    Page 500
                </a>
            </li>
        </ul>
    </li> -->
    
    <!-- <li class="left-menu-list-separator"></li>
    <li>
        <a class="left-menu-link" href="apps-profile.html">
            <i class="left-menu-link-icon icmn-profile"></i>
            Profile
        </a>
    </li> -->        
    <li class="left-menu-list-{{ (request()->is('meetings')) ? 'active' : '' }}">
        <a class="left-menu-link" href="/meetings">
            <i class="left-menu-link-icon fa fa-institution"></i>
            Sidang
        </a>
    </li>
    @if(\Auth::user()->user_type_id == 1)
    <li class="left-menu-list-separator "><!-- --></li>    
    <li class="left-menu-list-submenu">
        <a class="left-menu-link" href="javascript: void(0);">
            <i class="left-menu-link-icon icmn-cog util-spin-delayed-pseudo"><!-- --></i>
            Setting Page
        </a>
        <ul class="left-menu-list list-unstyled">
            <li>
                <a class="left-menu-link" href="/users">
                    <i class="counter-icon icmn-user"></i>
                    Mengelola User
                </a>
            </li>
            <li>
                <a class="left-menu-link" href="/user_types">
                    <i class="counter-icon icmn-users"></i>
                    Mengelola Tipe User 
                </a>
            </li>            
        </ul>   
    </li>
    @endif
</ul>