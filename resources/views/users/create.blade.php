@extends('layouts.app')

@section('content')
<!-- Basic Form Elements -->
<section class="panel">
    <div class="panel-heading">
        <h3>Tambah User</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="margin-bottom-50">
                    <h4>Form User</h4>
                    <br />
                    <!-- Horizontal Form -->                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('users') }}" enctype="multipart/form-data">
                         @csrf
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Nama User</label>
                            </div>
                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  placeholder="Input Name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Tipe User</label>
                            </div>
                            <div class="col-md-9">
                                <select id="user_type_id" type="text" class="selectpicker form-control{{ $errors->has('user_type_id') ? ' is-invalid' : '' }}" name="user_type_id"  placeholder="Input user_type_id" data-live-search="true" required autofocus>
                                    <option>Pilih Tipe User</option>
                                    @foreach ($user_types as $p)
                                    <option value="{{ $p->id }}">{{ $p->name }}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('user_type_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user_type_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l0">Gambar User</label>
                            </div>
                            <div class="col-md-9">
                                <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image"  placeholder="Input Gambar User" autofocus>

                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l2">Email Address</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icmn-mail2"></i>
                                    </span>
                                    <input id="l2" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"  placeholder="Email Address" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l3">Password</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input id="l3" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  placeholder="Input Password" required autofocus>
                                    <span class="input-group-addon">
                                        <i class="icmn-key"></i>
                                    </span>
                                </div>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="form-control-label" for="l3">Confirm Password</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">                                    
                                    <input id="l4" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password-confirm"  placeholder="Confirm Password" required autofocus>
                                    <span class="input-group-addon">
                                        <i class="icmn-key"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                        
                         <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>
                                <a class="btn btn-success" href="/users">Back</a>
                            </div>
                        </div>
                    </form>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>        
</section>
<!-- End -->
@endsection