<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home');
// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return redirect('/');
})->name('dashboard');

// route users
Route::resource('users', 'App\Http\Controllers\UsersController');    
Route::get('/users/delete/{id}', 'App\Http\Controllers\UsersController@destroy');

// route user_types
Route::resource('user_types', 'App\Http\Controllers\UserTypesController');    
Route::get('/user_types/delete/{id}', 'App\Http\Controllers\UserTypesController@destroy');

// route meetings
Route::resource('meetings', 'App\Http\Controllers\MeetingsController');    
Route::get('/meetings/delete/{id}', 'App\Http\Controllers\MeetingsController@destroy');
Route::get('/meetings/create_document_meetings/{id}', 'App\Http\Controllers\MeetingsController@create_document_meetings');
Route::post('/meetings/create_document_meetings', 'App\Http\Controllers\MeetingsController@store_document_meetings');
Route::post('/meetings/update_document_report', 'App\Http\Controllers\MeetingsController@update_document_report');
Route::get("/meetings/download_document_meetings/{id}", "App\Http\Controllers\MeetingsController@download_document_meetings");
Route::get("/meetings/delete_meeting_document/{id}", "App\Http\Controllers\MeetingsController@delete_meeting_document");
Route::get('/meetings/{id}/export_excel', 'App\Http\Controllers\MeetingsController@export_excel');
Route::get("/meetings/download_document_report/{id}", "App\Http\Controllers\MeetingsController@download_document_report");

// route meetings
Route::resource('meeting_agendas', 'App\Http\Controllers\MeetingAgendasController');    
Route::get('/meeting_agendas/delete/{id}', 'App\Http\Controllers\MeetingAgendasController@destroy');

// route Comments
Route::resource('comments', 'App\Http\Controllers\CommentsController');    
Route::get('/comments/delete/{id}', 'App\Http\Controllers\CommentsController@destroy');
Route::post('/comments/update_meeting_agenda', 'App\Http\Controllers\CommentsController@update_meeting_agenda');
