# Aplikasi Sigesit

Aplikasi Pencatatan Kertas Posisi Sidang
=======
## Aplikasi Sigesit

### Development Environtment
1. Nginx / Apache
2. PHP 7.3
3. PHP-mbstring for encryption
4. Laravel Framework 8.0
5. Mysql 

### Developer Installation
1. Install [Composer](https://getcomposer.org/)
2. Git Clone package ini (https://gitlab.com/bluez_dis/pj-sigesit-laravel.git)
3. Masuk ke dalam folder package 

4. Copy .env.example ke file .env
5. Isi .env pada bagian DB_* dengan konfigurasi yang benar
6. Jalankan perintah

```` shell
chmod -R 777 storage
chmod -R 777 bootstrap
chmod -R 777 public
composer install --optimize-autoloader --no-dev
php artisan config:cache
php artisan route:cache
chgrp -R www-data storage bootstrap/cache
chmod -R ug+rwx storage bootstrap/cache
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed
````
8. Arahkan webroot ke folder public
9. Buka browser, lalu arahkan alamat ke webroot yang sudah di-set ke folder public
