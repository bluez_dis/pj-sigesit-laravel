<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('user_types')->insert([
            'id' => '1',
            'name' => 'admin',            
            'created_by' => 1,
        ]);
        DB::table('user_types')->insert([
            'id' => '2',
            'name' => 'User',
            'created_by' => 1,
        ]);
        
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'email_admin@gmail.com',
            'password' => bcrypt('admin123'),
            'user_type_id' => '1',
            'image' => 'uploads/attachment/users//1603642668-admin.jpg'
        ]);

        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'email_user@gmail.com',
            'password' => bcrypt('user12345'),
            'user_type_id' => '2',
            'image' => 'uploads/attachment/users//1603642706-1-gambar.jpg'
        ]);


    }
}
