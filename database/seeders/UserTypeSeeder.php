<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->insert([
            'id' => '1',
            'name' => 'admin',            
            'created_by' => 1,
        ]);
        DB::table('user_types')->insert([
            'id' => '2',
            'name' => 'User',
            'created_by' => 1,
        ]);
    }
}
